Quando é encontrado um defeito de design do bootstrap, deve ser tratado da seguinte forma:

Criar uma issue com o título: def-design-bootstrap  (issue do requisito)

Corpo da issue:

- Identificar a imagem ou vista em questão

- Especificar o problema ou os objetos em falta

- Enviar uma screenshot do erro


Assign a issue ao líder de Design.

E determinar quantas horas deverá levar a corrigir o defeito. Por exemplo:

Tempo estimado para desenvolvimento da especificação: (escrever o tempo)
