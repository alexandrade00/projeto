# Estrutura para um issue relacionado com testes

## Testes - Requisito [num_requisito]

- Para fazer os testes funcionais ("black box"), é necessário correr a aplicação e verificar se o que fo implementado está de acordo com o requisito em questão.

## Testes de: [num_issue_requisito]

####  Descrição

- Fazer uma pequena descrição, por etapas, de como foi feito o teste.
