
# Estrutura para um issue relacionado com testes

## Testes - Requisito [num_requisito]

## Requisito: [num_requisito] #[num_issue_do_requisito]

#### Elementos a testar

- Identificar os elementos a testar do requisito

#### Instruções

- Dar instruções, passo a passo, de como foi feito o teste
