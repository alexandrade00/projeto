
## Code Formatter

```black .```

## Static Analysis
```pylint --fail-under=7 ./**/```


## Complexity Analysis
```radon cc -s -a ./**/```

```radon mi -s ./**/ ```

```radon raw ./**/```

```radon hal ./**/ ```


## Testing
Mudar para a pasta dev/src/
```pytest```
