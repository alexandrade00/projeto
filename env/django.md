O django vai ser a framework backend que utilizaremos, o django deve ser instalado seguindo estas instruções: https://docs.djangoproject.com/en/3.2/intro/install/. Deve ser criada uma venv na pasta onde irão alocar o projeto.

Qualquer dúvida que tenha sobre uma funcionalidade do django pode consultar a documentação, https://docs.djangoproject.com/en/3.2/.

Vídeo de instalaçao do django com os básicos: https://www.youtube.com/watch?v=F5mRW0jo-U4&t=8356s (até 2h15).