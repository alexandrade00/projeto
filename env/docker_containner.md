# Docker

Para garantir um ambiente de desenvolvimento igual para todos e para simplificar futuras alterções de versões do python ou até módulos, são usados 2 containners de Docker. Um corre o codigo em python, do django, relacionado com o web server, e o outro corre a base de dados MongoDB.

## Instalação

- Seguir os passos de instalação para o OS em questão no site do [Docker](https://docs.docker.com/get-docker/)

- Em linux / MacOS, executar o ficheiro run.sh

- Em Windows, correr o comando `docker-compose -f docker-compose.yml up --build` para instalar os containner e o comando `docker-compose up` para correr

## Correr os containners

- Em linux / MacOS, executar o ficheiro run.sh

- Em Windows, executar `docker-compose up` para correr