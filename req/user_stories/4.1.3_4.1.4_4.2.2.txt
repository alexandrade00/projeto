4.1.3: Eu, como utilizador autenticado, após seleção do repositório, bem como do branch, quero ver a linha de tempo distríbuida uniformemente por eventos temporais no repositório, de modo a facilitar a visualização de alterações a ficheiros em ficheiros raramente alterados.

4.1.4: Eu, como utilizador autenticado, após seleção do repositório, bem como do branch, quero (caso seja possível) poder ampliar, ou reduzir a linha de tempo vísivel para ter uma melhor perspetiva de uma série de eventos localizada.

4.2.1: Eu, como utilizador autenticado, quero ver a árvore  de ficheiros do repositório, para ter uma visão clara da estrutura/do esqueleto do projeto.

4.2.2:	.1: Como utilizador autenticado, ao ver a árvore de diretorias/ficheiros do repositório deverei ter a possibilidade  de ver os ficheiros 		presentes em cada pasta, com nome e extensão. Deste modo cada tipo de diretoria deve ser distinta das restantes (REQ, DESIGN, CODE, TEST, CONFIG, etc.. ).

		.2: Para além disto, a vista dos ficheiros presentes em cada pasta deve ser ser vista por algum tipo de "churn". Assim facilita-se a visualização de ficheiros que podem ter mais problemas, por estarem a sofrer atualizações regulares.

		.3: Pretendo ainda que, ao selecionar um ficheiro, todo o conteúdo desse seja visível no momento desse commit.