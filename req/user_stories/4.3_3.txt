Como membro do projeto
quero aceder a informação estática do ficheiro, i.e: o tipo (REQ/TST...), e ainda a dimensão do mesmo. O acesso também deve ter incluido as métricas de estabilidade e ainda, caso o ficheiro for código, a sua complexidade.
Deste modo terei acesso a diversos parâmetros relativos à informação estática dos ficheiros, tendo então disponível mais informações sobre os mesmos.
