# Ficheiros usados para criar os ficheiros com issues associados a cada autor

## Número de issues em que cada autor participa

- Alexandre Andrade -> 137
- Simão Monteiro -> 25
- João Silva -> 68
- José Silva -> 46
- Paulo Meira -> 22
- Noémia Gonçalves -> 34
- João Dionísio -> 23
- Davide Areias -> 59
- Rui Costa -> 21
- Rui Moniz -> 3
- Nuno Seixas -> 3
- Tomás Mendes -> 116
- Joel Oliveira -> 60
- Antonio Damasceno -> 3
- Ricardo Simões -> 22
- Gonçalo Ferreira -> 16
- Rafael Silva -> 3
- Marco Pais -> 21
- Mário ZR -> 4
- José Pedro Braz -> 3
- André Graça -> 28
- Telmo Correia -> 2

## Nota

Na pasta autores encontra-se uma descrição individual dos issues em que cada um participa
