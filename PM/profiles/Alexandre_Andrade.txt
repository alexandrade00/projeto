Nome: Alexandre Domingues Andrade
Nº estudante: 2019220216
E-mail: alexandrade@student.dei.uc.pt
Cargos: Gestor de Projeto, Gestor de Desenvolvimento, Gestor de Produto, Developer

Tempo dispendido: 78h30min
Issues:
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/300
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/298
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/296
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/294
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/285
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/281
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/280
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/272
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/271
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/270
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/269
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/267
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/218
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/266
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/261
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/245
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/244
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/243
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/242
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/238
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/181
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/223
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/217
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/207
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/205
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/201
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/189
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/188
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/184
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/180
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/170
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/134
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/154
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/153
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/152
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/147
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/145
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/158
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/128
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/126
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/104
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/101
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/100
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/99
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/92
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/87
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/88
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/93
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/82
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/80
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/75
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/74
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/70
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/69
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/64
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/48
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/33
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/23
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/18
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/22
https://gitlab.com/engenharia-de-software-pl5/projeto/-/issues/4
