from os import getenv
from dotenv import load_dotenv
from pymongo import MongoClient

load_dotenv()


def get_database():
    """Devolve uma conexão à base de dados utilizada no projeto"""
    load_dotenv()
    client = MongoClient(getenv("DATABASE_URL"))
    return client["repdb"]


def get_repo_collection(project_id: int):
    """Devolve uma conexão à coleção project_id"""
    data_base = get_database()
    mongo_collection = data_base[str(project_id)]
    return mongo_collection


def is_file_or_directory(project_id: int, directory: str):

    mongo_collection = get_repo_collection(project_id)
    file = mongo_collection.find_one(
        {"feature_id": 10, "files.name": {"$eq": directory[5:]}}
    )

    if file == None:
        return "directory"
    else:
        return "file"
