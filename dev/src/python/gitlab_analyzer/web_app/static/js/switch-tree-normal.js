document.getElementById("switch-btn").onclick = function () {
  if (document.getElementById("table").style.display !== "none") {
    document.getElementById("table").style.display = "none";
    document.getElementById("tree").style.display = "block";
    document.getElementById("switch-btn").innerText = "Show Normal";
  } else {
    document.getElementById("table").style.display = "block";
    document.getElementById("tree").style.display = "none";
    document.getElementById("switch-btn").innerText = "Show Tree";
  }
};
document.getElementById("tree").style.display = "none";
