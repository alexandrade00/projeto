import pytest
from django.urls import reverse


# @pytest.mark.skip
@pytest.mark.parametrize("projeto_id", [("abcdefghijklmnop"), ("'$%&//()!?[]\{\}")])
def test_not_numeric_homepage(client, projeto_id):
    form_url = reverse("homepage")
    response = client.post(form_url, {"projeto_id": projeto_id})
    assert "Bad Input" in response.context["error"]


# @pytest.mark.skip
def test_valid_input(client):
    form_url = reverse("homepage")
    response = client.post(form_url, {"projeto_id": 29777940})
    assert response.status_code == 302  # Redirect em caso de input valido


@pytest.mark.parametrize("projeto_id", [("123"), (123)])
def test_invalid_project_id(client, projeto_id):
    form_url = reverse("homepage")
    response = client.post(form_url, {"projeto_id": projeto_id})
    print(response.context)
    assert "Projeto não encontrado" in response.context["error"]
    # assert 1 == 1


# @pytest.mark.skip
@pytest.mark.parametrize("projeto_id", [("123"), (123)])
def test_not_numeric_dashboard(client, projeto_id):
    response = client.post(f"/project/{projeto_id}/")
    print(response.context)
    assert "Projeto não encontrado" in response.context["error"]
