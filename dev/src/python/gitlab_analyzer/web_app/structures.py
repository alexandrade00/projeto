from typing import Callable


class BackStack:
    """
    Classe que mantém o histórico de vistas numa pilha.

    ...

    Attributes
    ----------
    stack : list[ str ]
            lista com o histórico de urls
    previous_url : str
            string com o ultimo url visitado
    is_stack : boolean
            boolean para verificar se o site está a ser carregado através da pilha ou não"""

    def __init__(self):
        """
        Construtor da classe

        Parameters
        ----------
        """
        self._stack: list[str] = []
        self._previous_url: str = None
        self.is_stack: bool = False

    def pop(self):
        """Retira, e devolve, o ultimo elemento inserido na pilha"""
        try:
            data = self._stack.pop()
            if len(self._stack) == 0:
                self._previous_url = None
            return data
        except:
            pass

    def push(self, url: str):
        """Insere o ultimo url acedido na pilha"""
        if not self.is_first() and not self.is_stack:
            self._stack.append(self._previous_url)
        self.is_stack = False
        self._previous_url = url

    def set_previous(self, data: str):
        """Guarda o ultimo url acedido em memória"""
        self._previous_url = str

    def is_first(self):
        """Verifica se há algum url acedido anteriormente"""
        return self._previous_url is None

    def __len__(self):
        """Devolve o numero de elementos na pilha"""
        return len(self._stack)

    def __str__(self):
        """Converte a pilha para string"""
        return str(self._stack) + "\n"
